﻿using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class DailyInfoWidget : MonoBehaviour
    {
        [SerializeField] Text _dayText;
        [SerializeField] Image _weatherImage;
        [SerializeField] Text _tempMaxText;
        [SerializeField] Text _tempMinText;
        [SerializeField] Text _dateText;

        public void Initialize()
        { 
            _dayText = transform.GetChild(0).GetComponent<Text>();
            _weatherImage = transform.GetChild(1).GetComponent<Image>();
            _tempMinText = transform.GetChild(2).GetComponent<Text>();
            _tempMaxText = transform.GetChild(3).GetComponent<Text>();
            _dateText = transform.GetChild(4).GetComponent<Text>();
        }

        public void UpdateInfo(string day, float tempMin, float tempMax, string icon, string date)
        {
            _tempMinText.text = Mathf.Round(tempMin) + "°C";
            _tempMaxText.text = Mathf.Round(tempMax) + "°C";
            _dayText.text = day;
            _dateText.text = date;

            switch (icon)
            {
                case ("01d"):
                    _weatherImage.sprite = Manager.Data._01d;
                    break;
                case ("01n"):
                    _weatherImage.sprite = Manager.Data._01n;
                    break;
                case ("02d"):
                    _weatherImage.sprite = Manager.Data._02n;
                    break;
                case ("02n"):
                    _weatherImage.sprite = Manager.Data._02n;
                    break;
                case ("03d"):
                    _weatherImage.sprite = Manager.Data._03d;
                    break;
                case ("03n"):
                    _weatherImage.sprite = Manager.Data._03n;
                    break;
                case ("04d"):
                    _weatherImage.sprite = Manager.Data._04d;
                    break;
                case ("04n"):
                    _weatherImage.sprite = Manager.Data._04n;
                    break;
                case ("09d"):
                    _weatherImage.sprite = Manager.Data._09d;
                    break;
                case ("09n"):
                    _weatherImage.sprite = Manager.Data._09n;
                    break;
                case ("10d"):
                    _weatherImage.sprite = Manager.Data._10d;
                    break;
                case ("10n"):
                    _weatherImage.sprite = Manager.Data._10n;
                    break;
                case ("11d"):
                    _weatherImage.sprite = Manager.Data._11d;
                    break;
                case ("11n"):
                    _weatherImage.sprite = Manager.Data._11n;
                    break;
                case ("13d"):
                    _weatherImage.sprite = Manager.Data._13d;
                    break;
                case ("13n"):
                    _weatherImage.sprite = Manager.Data._13n;
                    break;
                case ("50d"):
                    _weatherImage.sprite = Manager.Data._50d;
                    break;
                case ("50n"):
                    _weatherImage.sprite = Manager.Data._50n;
                    break;
                default:
                    _weatherImage.sprite = null;
                    break;
            }
        }
    }
}
