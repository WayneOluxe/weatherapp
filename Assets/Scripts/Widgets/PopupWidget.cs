﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class PopupWidget : MonoBehaviour
    {

        [SerializeField] Button _confirmButton;

        public void OnClick(Button button)
        {
            if (button == _confirmButton)
                Manager.Popup.DisablePopup();
        }
    }
}
