﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class ResultWidget : MonoBehaviour
    {
        [SerializeField] Text _location;
        [SerializeField] Text _lat;
        [SerializeField] Text _lon;
        [SerializeField] Button _add;

        public void Initialize()
        {
            _location = transform.GetChild(0).GetComponent<Text>();
            _lat = transform.GetChild(1).GetComponent<Text>();
            _lon = transform.GetChild(2).GetComponent<Text>();
            _add = transform.GetChild(3).GetComponent<Button>();

            _add.onClick.AddListener(delegate { AddResult(); });
        }

        public void UpdateInfo(string location, string lat, string lon)
        {
            _location.text = location;
            _lat.text = lat;
            _lon.text = lon;
        }

        public void AddResult()
        {
            print(name);

            Manager.App.AddSubLocation(_location.text, _lat.text, _lon.text);
        }
    }
}
