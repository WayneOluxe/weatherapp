﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class RegionWidget : MonoBehaviour
    {

        [SerializeField] Text _locationText;
        [SerializeField] Text _latText;
        [SerializeField] Text _lonText;
        [SerializeField] Button _removeButton;

        public string location { get; private set; }

        public void Initialize()
        {
            _locationText = transform.GetChild(0).GetComponent<Text>();
            _latText = transform.GetChild(1).GetComponent<Text>();
            _lonText = transform.GetChild(2).GetComponent<Text>();
            _removeButton = transform.GetChild(3).GetComponent<Button>();

            _removeButton.onClick.AddListener(delegate { Manager.Popup.EnableWarningPopup(this); });
        }

        public void UpdateInfo(string _location, string _lat, string _lon)
        {
            _locationText.text = _location;
            _latText.text = _lat;
            _lonText.text = _lon;
            location = _location;
        }
    }
}
