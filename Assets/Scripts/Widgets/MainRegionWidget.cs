﻿using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class MainRegionWidget : MonoBehaviour
    {

        [SerializeField] Text _location;
        [SerializeField] Text _lat;
        [SerializeField] Text _lon;

        void Start()
        {
            
        }
        
        public void UpdateInfo()
        {
            _location.text = Manager.Data.weatherData[0].location;
            _lat.text = Manager.Data.weatherData[0].latitude.ToString();
            _lon.text = Manager.Data.weatherData[0].longitude.ToString();
        }
    }
}