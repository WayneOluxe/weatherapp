﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class WeatherInfoWidget : MonoBehaviour
    {

        [SerializeField] Text _cityName;
        [SerializeField] Text _temperature;
        [SerializeField] Text _weatherText;
        [SerializeField] Image _weatherImage;
         
        public void Initialize()
        {
            _weatherImage = transform.GetChild(0).GetComponent<Image>();
            _cityName = transform.GetChild(1).GetComponent<Text>();
            _temperature = transform.GetChild(2).GetComponent<Text>();
            _weatherText = transform.GetChild(3).GetComponent<Text>();
        }

        public void UpdateInfo(string cityName, float temperature, string weather, string icon)
        {
            _cityName.text = cityName;
            _temperature.text = Mathf.Round(temperature) + "°C";
            _weatherText.text = weather;
            switch (icon)
            {
                case ("01d"):
                    _weatherImage.sprite = Manager.Data._01d;
                    break;
                case ("01n"):
                    _weatherImage.sprite = Manager.Data._01n;
                    break;
                case ("02d"):
                    _weatherImage.sprite = Manager.Data._02n;
                    break;
                case ("02n"):
                    _weatherImage.sprite = Manager.Data._02n;
                    break;
                case ("03d"):
                    _weatherImage.sprite = Manager.Data._03d;
                    break;
                case ("03n"):
                    _weatherImage.sprite = Manager.Data._03n;
                    break;
                case ("04d"):
                    _weatherImage.sprite = Manager.Data._04d;
                    break;
                case ("04n"):
                    _weatherImage.sprite = Manager.Data._04n;
                    break;
                case ("09d"):
                    _weatherImage.sprite = Manager.Data._09d;
                    break;
                case ("09n"):
                    _weatherImage.sprite = Manager.Data._09n;
                    break;
                case ("10d"):
                    _weatherImage.sprite = Manager.Data._10d;
                    break;
                case ("10n"):
                    _weatherImage.sprite = Manager.Data._10n;
                    break;
                case ("11d"):
                    _weatherImage.sprite = Manager.Data._11d;
                    break;
                case ("11n"):
                    _weatherImage.sprite = Manager.Data._11n;
                    break;
                case ("13d"):
                    _weatherImage.sprite = Manager.Data._13d;
                    break;
                case ("13n"):
                    _weatherImage.sprite = Manager.Data._13n;
                    break;
                case ("50d"):
                    _weatherImage.sprite = Manager.Data._50d;
                    break;
                case ("50n"):
                    _weatherImage.sprite = Manager.Data._50n;
                    break;
                default:
                    _weatherImage.sprite = null;
                    break;
            }
        }
    }
}
