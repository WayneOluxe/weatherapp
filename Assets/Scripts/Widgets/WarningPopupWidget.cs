﻿using UnityEngine;
using WeatherApp.Managers;

namespace WeatherApp.Widgets
{
    public class WarningPopupWidget : MonoBehaviour
    {
        public void Decline()
        {
            Manager.Popup.DisableWarningPopup();
        }

        public void Accept()
        {
            Manager.Popup.WarningPopupAccept();
        }
    }
}
