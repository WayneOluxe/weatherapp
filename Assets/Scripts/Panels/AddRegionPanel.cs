﻿using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Managers;
using WeatherApp.Widgets;

namespace WeatherApp.Panels
{
    public class AddRegionPanel : MonoBehaviour
    {
        [SerializeField] InputField _inputField;
        [SerializeField] Button _searchButton;
        [SerializeField] GameObject _resultPanelPrefab;
        [SerializeField] GameObject _resultParent;
        [SerializeField] GameObject _searchPanel;
        [SerializeField] GameObject _addRegionButton;
        public MainRegionWidget _mainRegion;

        #region internals

        public void Search()
        {
            ClearSearchContents();
            Manager.App.StartSearch(_inputField.text);
        }

        public void CloseSearchPanel()
        {
            ClearSearchContents();
            Manager.App.UpdateRegionDisplay();

            _searchPanel.SetActive(false);
        }

        public void OpenSearchPanel()
        {
            _inputField.text = string.Empty;
            _searchPanel.SetActive(true);
        }

        public void CloseRegionPanel()
        {
            Manager.App.JumpToFirst();
            gameObject.SetActive(false);
            Manager.Popup.EnableSpinner();
            Manager.App.RefreshQuery();
            Manager.App.UpdateDisplay(0);
        }

        #endregion

        #region internals

        void ClearSearchContents()
        {
            foreach (Transform child in _resultParent.transform)
            {
                if (child.name != "HeaderPanel")
                {
                    Destroy(child.gameObject);
                }
            }

            _resultParent.GetComponent<RectTransform>().sizeDelta = new Vector2(_resultParent.GetComponent<RectTransform>().sizeDelta.x, 216f);
        }

        #endregion
    }
}
