﻿using System;
using System.Collections.Generic;

namespace WeatherApp.Data
{
    [Serializable]
    public class WeatherData
    {
        public string location;
        public string weather;
        public float latitude;
        public float longitude;
        public float temperature;
        public string icon;

        public List<HourlyData> hourlyData;
        public List<DailyData> dailyData;

        public WeatherData()
        {
            location = string.Empty;
            weather = string.Empty;
            latitude = 0f;
            longitude = 0f;
            temperature = 0f;
            icon = string.Empty;

            hourlyData = new List<HourlyData>();
            dailyData = new List<DailyData>();
        } 

        public void CreateDefault()
        {
            location = "Osaka";
            weather = "Sunny";
            latitude = 34.652500f;
            longitude = 135.506302f;
            temperature = 20f;
            icon = "01d";
        }

        public void Load(string _location, string _weather, float _lat, float _lon, float _temp, string _icon)
        {
            location = _location;
            weather = _weather;
            latitude = _lat;
            longitude = _lon;
            temperature = _temp;
            icon = _icon; 
        }

        public void LoadHourly(List<HourlyData> list)
        {
            hourlyData = list;
        }

        public void LoadDaily(List<DailyData> list)
        {
            dailyData = list;
        }
    }
}
