﻿using System;

namespace WeatherApp.Data
{
    [Serializable]
    public class DailyData
    {
        public string day;
        public float temperatureMax;
        public float temperatureMin;
        public string icon;
        public string date;

        public DailyData()
        {
            day = string.Empty;
            temperatureMax = 0f;
            temperatureMin = 0f;
            icon = string.Empty;
            date = string.Empty;
        }

        public DailyData(string _day, float _tempMax, float _tempMin, string _icon, string _date)
        {
            day = _day;
            temperatureMax = _tempMax;
            temperatureMin = _tempMax;
            icon = _icon;
            date = _date;
        }

        public void CreateDefault()
        {
            day = string.Empty;
            temperatureMin = 0f;
            temperatureMax = 0f;
            icon = "01d";
            date = string.Empty;
        }

        public void Load(string _day, float _tempMax, float _tempMin, string _icon, string _date)
        {
            day = _day;
            temperatureMax = _tempMax;
            temperatureMin = _tempMin;
            icon = _icon;
            date = _date;
        }
    }
}
