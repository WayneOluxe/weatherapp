﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeatherApp.Managers;

namespace WeatherApp.Data
{
    [Serializable]
    public class DataWrapper
    {
        public List<WeatherData> data;

        public void ImportData()
        {
            data = Manager.Data.weatherData;
        }

        public void ExportData()
        {
            Manager.Data.LoadData(data);
        }
    }
}
