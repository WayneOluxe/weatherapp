﻿using System;

namespace WeatherApp.Data
{
    [Serializable]
    public class HourlyData
    {
        public string hour;
        public float temperature;
        public string icon;
        public string day;

        public HourlyData()
        {
            hour = string.Empty;
            temperature = 0f;
            icon = string.Empty;
            day = string.Empty;
        }

        public HourlyData(string _hour, float _temp, string _icon, string _day)
        {
            hour = _hour;
            temperature = _temp;
            icon = _icon;
            day = _day;
        }

        public void CreateDefault()
        {
            hour = "0:00pm";
            temperature = 20f;
            icon = "01d";
            day = string.Empty;
        }

        public void Load(string _hour, float _temp, string _icon, string _day)
        {
            hour = _hour;
            temperature = _temp;
            icon = _icon;
            day = _day;
        }
    }
}
