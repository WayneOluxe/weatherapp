﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeatherApp.Managers
{
    public class Manager : MonoBehaviour
    {
        static PopupManager _popup;
        public static PopupManager Popup { get { return _popup; } }

        static AppManager _app;
        public static AppManager App { get { return _app; } }

        static AudioManager _audio;
        public static AudioManager Audio { get { return _audio; } }

        static DataManager _data;
        public static DataManager Data { get { return _data; } }

        void Awake()
        {
            _popup = gameObject.GetComponentInChildren<PopupManager>();
            _app = gameObject.GetComponentInChildren<AppManager>();
            _audio = gameObject.GetComponentInChildren<AudioManager>();
            _data = gameObject.GetComponentInChildren<DataManager>();
        }
    }
}