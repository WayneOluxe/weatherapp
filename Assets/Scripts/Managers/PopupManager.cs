﻿using UnityEngine;
using UnityEngine.UI;
using WeatherApp.Widgets;

namespace WeatherApp.Managers
{
    public class PopupManager : MonoBehaviour
    {
        [SerializeField] Text _headerText;
        [SerializeField] Text _bodyText;
        [SerializeField] GameObject _popup;
        [SerializeField] GameObject _loadingSpinner;
        [SerializeField] GameObject _warningPopup;
        [SerializeField] RectTransform _regionParent;

        RegionWidget _region;

        void Awake()
        {
            _popup.SetActive(false);
            _headerText.text = "エラー";
            _bodyText.text = "エラーメッセージ";
        }
        
        public void EnablePopup(string header, string body)
        {
            _headerText.text = header;
            _bodyText.text = body;
            _popup.SetActive(true);
            DisableSpinner();
        }

        public void DisablePopup()
        {
            _popup.SetActive(false);
        }

        public void EnableSpinner()
        {
            print("Enabling spinner...");
            _loadingSpinner.SetActive(true);
        }

        public void DisableSpinner()
        {
            print("Disabling spinner...");
            _loadingSpinner.SetActive(false);
        }

        public void EnableWarningPopup(RegionWidget region)
        {
            _region = region;
            _warningPopup.SetActive(true);
            DisableSpinner();
        }

        public void WarningPopupAccept()
        {
            _warningPopup.SetActive(false);
            EnableSpinner();
            Manager.Data.DeleteEntry(_region.location);
            Destroy(_region.gameObject);
            //_regionParent.sizeDelta = new Vector2(_regionParent.sizeDelta.x, _regionParent.sizeDelta.y - 216f);
        }

        public void DisableWarningPopup()
        {
            _region = null;
            _warningPopup.SetActive(false);
        }
    }
}
