﻿using System.Collections.Generic;
using UnityEngine;
using WeatherApp.Data;
using WeatherApp.Widgets;

namespace WeatherApp.Managers
{
    public class DataManager : MonoBehaviour
    {
        public List<WeatherData> weatherData = new List<WeatherData>();
        public List<HourlyData> hourlyData = new List<HourlyData>();
        public List<DailyData> dailyData = new List<DailyData>();

        #region WeatherIcons

        public Sprite _01d;
        public Sprite _01n;
        public Sprite _02d;
        public Sprite _02n;
        public Sprite _03d;
        public Sprite _03n;
        public Sprite _04d;
        public Sprite _04n;
        public Sprite _09d;
        public Sprite _09n;
        public Sprite _10d;
        public Sprite _10n;
        public Sprite _11d;
        public Sprite _11n;
        public Sprite _13d;
        public Sprite _13n;
        public Sprite _50d;
        public Sprite _50n;

        #endregion

        #region Unity

        void Awake()
        {
        }

        #endregion

        #region Weather Data

        public void AddEntry(WeatherData weatherData)
        {

            bool isClash = false;
            foreach(WeatherData data in this.weatherData)
            {
                if(data.location == weatherData.location)
                {
                    Manager.Popup.EnablePopup("エラー", "ロケーション情報は既に存在しました");
                    isClash = true;
                }
                if (isClash)
                    break;
            }
            
            if (!isClash)
            {
                this.weatherData.Add(weatherData);
            }
        }

        public void DeleteEntry(string location)
        {
            bool foundEntry = false;
            foreach(WeatherData data in weatherData)
            {
                if (data.location == location)
                {
                    weatherData.Remove(data);
                    foundEntry = true;
                }
                if (foundEntry)
                    break;
            }
            Manager.Popup.DisableSpinner();
        }

        public void LoadData(List<WeatherData> list)
        {
            weatherData = list;
        }

        #endregion

        #region Hourly Data

        public void AddHourly(HourlyData data)
        {
            hourlyData.Add(data);
        }

        #endregion

        #region Daily Data

        public void AddDaily(DailyData data)
        {
            dailyData.Add(data);
        }

        #endregion
    }
}
