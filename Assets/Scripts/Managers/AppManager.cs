﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using UnityEngine;
using WeatherApp.Data;
using WeatherApp.Widgets;
using System;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using WeatherApp.Panels;
using System.IO;

namespace WeatherApp.Managers
{
    public class AppManager : MonoBehaviour
    {
        [SerializeField] RectTransform _weatherInfoParent;
        [SerializeField] RectTransform _hourlyInfoParent;
        [SerializeField] RectTransform _dailyInfoParent;
        [SerializeField] RectTransform _regionParent;
        [SerializeField] RectTransform _resultParent;

        [SerializeField] GameObject _weatherInfoPrefab;
        [SerializeField] GameObject _hourlyInfoPrefab;
        [SerializeField] GameObject _dailyInfoPrefab;
        [SerializeField] GameObject _regionPrefab;
        [SerializeField] GameObject _resultPrefab;

        [SerializeField] GameObject _regionPanel;

        [SerializeField] ScrollSnap _mainWeatherInfoScrollview;
 
        List<WeatherInfoWidget> _widgets = new List<WeatherInfoWidget>();

        string ApiAddress = "http://api.openweathermap.org/data/2.5/weather?mode=xml&units=metric&appid=fa274642c5786ea3a13109fdc4c2eeac&lat=";
        string HourlyApiAddress = "http://api.openweathermap.org/data/2.5/forecast?appid=fa274642c5786ea3a13109fdc4c2eeac&units=metric&mode=xml&lat=";
        string IconAddress = "http://openweathermap.org/img/w/";
        string GoogleApiAddress = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&latlng=";
        string GeonamesApiAddress = "http://api.geonames.org/search?maxRows=5&username=oluxe&q=";
        string FileName = "WeatherData.json";
        string filePath;

        string _cityName = string.Empty;
        string _weather = string.Empty;
        float _temp = 0f;
        float _lat = 34.68238f;
        float _lng = 135.4988f;
        string _iconName = string.Empty;
        int _scrollIndex = 0;
        int _currentIndex = 0;

        bool _firstScroll = true;
        bool _isAdd = false;

        WaitForSeconds waitTime = new WaitForSeconds(1);

        #region Unity

        void Awake()
        {
            Screen.orientation = ScreenOrientation.Portrait;
            _mainWeatherInfoScrollview.onPageChange += OnPageChange;
            filePath = Path.Combine(Application.streamingAssetsPath, FileName);

            _weatherInfoParent.sizeDelta = new Vector2(0f, _weatherInfoParent.rect.height);
            _hourlyInfoParent.sizeDelta = new Vector2(0f, _hourlyInfoParent.rect.height);
            _dailyInfoParent.sizeDelta = new Vector2(0f, _dailyInfoParent.rect.height);

            if (File.Exists(filePath))
                FromJson();
            else
                StartQuery();
        }

        #endregion

        #region Data
        // TODO : Remove all prints before final build

        void StartQuery()
        {
            Manager.Popup.EnableSpinner();
#if !UNITY_EDITOR
            StartCoroutine(GetLoc());
#else
            //Manager.Popup.EnablePopup("成功！", "Latitude : " + _lat.ToString() + " , " + "Longitude : " + _lng.ToString());
            //StartCoroutine(GetCity());
            StartCoroutine(GetWeather());
#endif
        }

        public void RefreshQuery()
        {
            _cityName = Manager.Data.weatherData[_scrollIndex].location;
            _lat = Manager.Data.weatherData[_scrollIndex].latitude;
            _lng = Manager.Data.weatherData[_scrollIndex].longitude;

            Manager.Data.hourlyData.Clear();
            Manager.Data.dailyData.Clear();

            StartCoroutine(GetWeather());
        }

        IEnumerator GetLoc()
        {
            // No location permission
            if (!Input.location.isEnabledByUser)
            {
                Manager.Popup.EnablePopup("エラー", "ロケーションサービスのご許可をお願いします");
                print("Permission error");
                yield break;
            }

            // Begin location query
            Input.location.Start();
            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
            {
                yield return waitTime;
                maxWait--;
            }

            // Timeout
            if (maxWait < 1)
            {
                Manager.Popup.EnablePopup("エラー", "タイムアウトです。電波状態をご確認してください");
                print("Timeout error");
                yield break;
            }

            // Connection failed
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                Manager.Popup.EnablePopup("エラー", "ロケーションデーター収集失敗。電波状態とロケーションサービス許可状態をご確認してください");
                print("Location grab failed");
                yield break;
            }
            else
            {
                // Query success
                _lat = Input.location.lastData.latitude;
                _lng = Input.location.lastData.longitude;
                //Manager.Popup.EnablePopup("成功！", "Latitude : " + _lat + " , " + "Longitude : " + _lng);
                StartCoroutine(GetWeather());
            }
            yield return null;
        }

        IEnumerator GetCity()
        {
            WWW www = new WWW(GoogleApiAddress + _lat + "," + _lng);
            yield return www;

            if (www.error == null)
            {
                XmlDocument data = new XmlDocument();
                data.LoadXml(www.text);

                bool cityFound = false;

                foreach (XmlNode eachAddressComponent in data.GetElementsByTagName("result").Item(1).ChildNodes)
                {
                    if (eachAddressComponent.Name == "address_component")
                    {
                        // Get element from xml node
                        foreach (XmlNode eachAddressAttribute in eachAddressComponent.ChildNodes)
                        {
                            if (eachAddressAttribute.Name == "short_name")
                                _cityName = eachAddressAttribute.FirstChild.Value;
                            if (eachAddressAttribute.Name == "type" && eachAddressAttribute.FirstChild.Value == "locality")
                                cityFound = true;
                        }
                        if (cityFound)
                            break;
                    }
                }

                StartCoroutine(GetWeather());
            }
            else
            {
                Manager.Popup.EnablePopup("エラー", www.error);
            }
        }

        IEnumerator GetWeather()
        {
            string url = ApiAddress + Math.Round(_lat, 1) + "&lon=" + Math.Round(_lng, 1);
            WWW www = new WWW(url);
            yield return www;

            if (www.error == null)
            {
                XmlDocument data = new XmlDocument();
                data.LoadXml(www.text);

                foreach(XmlNode node in data.GetElementsByTagName("current").Item(0).ChildNodes)
                {
                    if (node.Name == "weather")
                    {
                        _weather = node.Attributes.GetNamedItem("value").Value;

                        _iconName = node.Attributes.GetNamedItem("icon").Value;
                    }
                    else if (node.Name == "temperature")
                        _temp = float.Parse(node.Attributes.GetNamedItem("value").Value);
                    else if (node.Name == "city" && _cityName == string.Empty)
                        _cityName = node.Attributes.GetNamedItem("name").Value;
                    
                }
                
                StartCoroutine(GetHourly());
            }
            else
            {
                Manager.Popup.EnablePopup("エラー", www.error);
            }
        }

        IEnumerator GetHourly()
        {
            WWW www = new WWW(HourlyApiAddress + Math.Round(_lat, 1) + "&lon=" + Math.Round(_lng, 1));
            yield return www;

            if (www.error == null)
            {
                XmlDocument data = new XmlDocument();
                data.LoadXml(www.text);

                System.IO.File.WriteAllText("./testXML.txt", www.text);

                XmlNodeList forecasts = data.SelectNodes("//weatherdata/forecast/time");

                List<HourlyData> hourlyData = new List<HourlyData>();
                int hourlyCount = 0;

                DateTime currentDate = DateTime.Now;
                List<int> addedDays = new List<int>();

                foreach (XmlNode forecast in forecasts)
                {
                    //string time = forecast.Attributes.GetNamedItem("from").Value;
                    DateTime date = DateTime.Parse(forecast.Attributes.GetNamedItem("from").Value);
                    DateTime.SpecifyKind(date, DateTimeKind.Utc);
                    date = date.ToLocalTime();

                    XmlNode iconNode = forecast.FirstChild;
                    XmlNode temperature = forecast.LastChild.PreviousSibling.PreviousSibling.PreviousSibling;

                    if (date > currentDate && !addedDays.Contains(date.Day))
                    {
                        addedDays.Add(date.Day);
                        if (date.Day == currentDate.Day)
                        {
                            DailyData dData = new DailyData("Today", float.Parse(temperature.Attributes.GetNamedItem("max").Value), float.Parse(temperature.Attributes.GetNamedItem("min").Value), iconNode.Attributes.GetNamedItem("var").Value, date.Date.ToShortDateString());
                            Manager.Data.AddDaily(dData);
                        }
                        else
                        {
                            DailyData dData = new DailyData(date.DayOfWeek.ToString(), float.Parse(temperature.Attributes.GetNamedItem("max").Value), float.Parse(temperature.Attributes.GetNamedItem("min").Value), iconNode.Attributes.GetNamedItem("var").Value, date.Date.ToShortDateString());
                            Manager.Data.AddDaily(dData);
                        }
                        
                    }

                    if (date > currentDate && hourlyCount < 8)
                    {
                        HourlyData hData = new HourlyData(date.Hour.ToString(), float.Parse(temperature.Attributes.GetNamedItem("value").Value), iconNode.Attributes.GetNamedItem("var").Value, date.Date.ToString("ddd"));
                        Manager.Data.AddHourly(hData);
                        hourlyCount += 1;
                    }
                }
            }
            else
                print("Hourly query ended with " + www.error);

            ParseResults();
        }

        void ParseResults()
        {
            WeatherData data = new WeatherData();
            data.Load(_cityName, _weather, _lat, _lng, _temp, _iconName);
            List<HourlyData> hourly = new List<HourlyData>(Manager.Data.hourlyData);
            List<DailyData> daily = new List<DailyData>(Manager.Data.dailyData);

            data.LoadDaily(daily);
            data.LoadHourly(hourly);

            if (Manager.Data.weatherData.Count == 0 || _isAdd)
            {
                print("Adding...");
                Manager.Data.AddEntry(data);
                _isAdd = false;
            }
            else
            {
                print("Editing...");
                Manager.Data.weatherData[_scrollIndex] = data;
            }

            Manager.Data.hourlyData.Clear();
            Manager.Data.dailyData.Clear();

            ToJson();
            print(_scrollIndex);
            UpdateDisplay(_scrollIndex);
        }

        public void StartSearch(string query)
        {
            Manager.Popup.EnableSpinner();
            StartCoroutine(Search(query));
        }

        IEnumerator Search(string query)
        {
            WWW www = new WWW(GeonamesApiAddress + query);

            float timer = 0;
            float timeOut = 5;
            bool failed = false;
            print("Beginning search...");
            while (!www.isDone)
            {
                if (timer > timeOut)
                {
                    failed = true;
                    Manager.Popup.EnablePopup("Error!", "Timed out! Please check your internet connection.");
                    break;
                }
                timer += Time.deltaTime;
                yield return null;
            }
            if (failed) www.Dispose();
            print("Search Ended!");
            if(www.error == null)
            {
                XmlDocument result = new XmlDocument();
                result.LoadXml(www.text);
                XmlNodeList geoList = result.SelectNodes("//geonames/geoname");

                if (geoList.Count > 0)
                {
                    foreach (XmlNode geo in geoList)
                    {
                        string name = geo.FirstChild.NextSibling.InnerText;

                        _resultParent.sizeDelta = new Vector2(_resultParent.sizeDelta.x, _resultParent.sizeDelta.y + 216f);
                        GameObject prefab = Instantiate(_resultPrefab, _resultParent);
                        ResultWidget rWidget = prefab.AddComponent<ResultWidget>();
                        rWidget.Initialize();
                        rWidget.UpdateInfo(geo.FirstChild.NextSibling.InnerText, geo.FirstChild.NextSibling.NextSibling.InnerText, geo.FirstChild.NextSibling.NextSibling.NextSibling.InnerText);
                        Manager.Popup.DisableSpinner();
                    }
                }
                else
                {
                    Manager.Popup.EnablePopup("Error!", "No results found!");
                }
            }
            else
            {
                Manager.Popup.EnablePopup("エラー", www.error);
            }
        }

        public void AddSubLocation(string cityName, string lat, string lng)
        {
            _isAdd = true;
            Manager.Popup.EnableSpinner();
            _cityName = cityName;
            _lat = (float)Math.Round(float.Parse(lat), 2);
            _lng = (float)Math.Round(float.Parse(lng), 2);

            StartCoroutine(GetWeather());
        }

        void ToJson()
        {
            string jsonText = string.Empty;

            DataWrapper data = new DataWrapper();
            data.ImportData();

            string dataAsJson = JsonUtility.ToJson(data);
            File.WriteAllText(filePath, dataAsJson);    
        }

        void FromJson()
        {
            print("FromJSON");
            string dataAsJson = File.ReadAllText(filePath);

            DataWrapper data = new DataWrapper();
            data = JsonUtility.FromJson<DataWrapper>(dataAsJson);

            Manager.Data.LoadData(data.data);

            RefreshQuery();
            UpdateDisplay(_scrollIndex);
        }

        #endregion

        #region Display
        
        public void UpdateDisplay(int Index)
        {
            print(Index);
            // Clear all existing objects
            foreach(Transform child in _weatherInfoParent)
            {
                Destroy(child.gameObject);
            }

            foreach(Transform child in _hourlyInfoParent)
            {
                Destroy(child.gameObject);
            }

            foreach(Transform child in _dailyInfoParent)
            {
                Destroy(child.gameObject);
            }

            _hourlyInfoParent.sizeDelta = new Vector2(0f, _hourlyInfoParent.sizeDelta.y);
            _dailyInfoParent.sizeDelta = new Vector2(0f, _dailyInfoParent.sizeDelta.y);
            _weatherInfoParent.sizeDelta = new Vector2(0f, _weatherInfoParent.sizeDelta.y);

            WeatherData data = Manager.Data.weatherData[Index];

            foreach(WeatherData _mainData in Manager.Data.weatherData)
            {
                _weatherInfoParent.sizeDelta = new Vector2(_weatherInfoParent.rect.width + 1536f, _weatherInfoParent.rect.height);
                GameObject prefab = Instantiate(_weatherInfoPrefab, _weatherInfoParent);

                WeatherInfoWidget widget = prefab.AddComponent<WeatherInfoWidget>();
                widget.Initialize();
                widget.UpdateInfo(_mainData.location, _mainData.temperature, _mainData.weather, _mainData.icon);
            }

            foreach (HourlyData hData in data.hourlyData)
            {
                _hourlyInfoParent.sizeDelta = new Vector2(_hourlyInfoParent.rect.width + 250f, _hourlyInfoParent.rect.height);
                GameObject hPrefab = Instantiate(_hourlyInfoPrefab, _hourlyInfoParent);

                HourlyInfoWidget hWidget = hPrefab.AddComponent<HourlyInfoWidget>();
                hWidget.Initialize();
                hWidget.UpdateInfo(hData.hour, hData.temperature, hData.icon, hData.day);
            }

            foreach (DailyData dData in data.dailyData)
            {
                _dailyInfoParent.sizeDelta = new Vector2(_dailyInfoParent.rect.width + 768f, _dailyInfoParent.rect.height);

                GameObject dPrefab = Instantiate(_dailyInfoPrefab, _dailyInfoParent);

                DailyInfoWidget dWidget = dPrefab.AddComponent<DailyInfoWidget>();
                dWidget.Initialize();
                dWidget.UpdateInfo(dData.day, dData.temperatureMin, dData.temperatureMax, dData.icon, dData.date);
            }
            
            _hourlyInfoParent.transform.parent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;
            _dailyInfoParent.transform.parent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;
            Manager.Popup.DisableSpinner();
        }

        void SwapDisplay(int Index)
        {
            print(Index);
            // Clear all existing objects
            foreach (Transform child in _hourlyInfoParent)
            {
                Destroy(child.gameObject);
            }

            foreach (Transform child in _dailyInfoParent)
            {
                Destroy(child.gameObject);
            }

            _hourlyInfoParent.sizeDelta = new Vector2(0f, _hourlyInfoParent.sizeDelta.y);
            _dailyInfoParent.sizeDelta = new Vector2(0f, _dailyInfoParent.sizeDelta.y);

            WeatherData data = Manager.Data.weatherData[Index];

            foreach (HourlyData hData in data.hourlyData)
            {
                _hourlyInfoParent.sizeDelta = new Vector2(_hourlyInfoParent.rect.width + 250f, _hourlyInfoParent.rect.height);
                GameObject hPrefab = Instantiate(_hourlyInfoPrefab, _hourlyInfoParent);

                HourlyInfoWidget hWidget = hPrefab.AddComponent<HourlyInfoWidget>();
                hWidget.Initialize();
                hWidget.UpdateInfo(hData.hour, hData.temperature, hData.icon, hData.day);
            }

            foreach (DailyData dData in data.dailyData)
            {
                _dailyInfoParent.sizeDelta = new Vector2(_dailyInfoParent.rect.width + 768f, _dailyInfoParent.rect.height);

                GameObject dPrefab = Instantiate(_dailyInfoPrefab, _dailyInfoParent);

                DailyInfoWidget dWidget = dPrefab.AddComponent<DailyInfoWidget>();
                dWidget.Initialize();
                dWidget.UpdateInfo(dData.day, dData.temperatureMin, dData.temperatureMax, dData.icon, dData.date);
            }

            _hourlyInfoParent.transform.parent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;
            _dailyInfoParent.transform.parent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;
            _weatherInfoParent.transform.parent.parent.GetComponent<ScrollSnap>().UpdateListItemsSize();
            Manager.Popup.DisableSpinner();
        }

        public void UpdateRegionDisplay()
        {
            Transform addRegion = null;
            foreach(Transform child in _regionParent.transform)
            {
                if (child.name == "AddRegion")
                    addRegion = child.transform;

                if (child.name != "HeaderPanel" && child.name != "MainRegionPanel" && child.name != "SubPanel" && child.name != "AddRegion")
                {
                    Destroy(child.gameObject);
                }
            }

            _regionParent.sizeDelta = new Vector2(_regionParent.sizeDelta.x, 864f);

            _regionPanel.GetComponent<AddRegionPanel>()._mainRegion.UpdateInfo();

            for(int i=1; i < Manager.Data.weatherData.Count; i++)
            {
                _regionParent.sizeDelta = new Vector2(_regionParent.sizeDelta.x, _regionParent.sizeDelta.y + 216f);
                GameObject region = Instantiate(_regionPrefab, _regionParent);
                RegionWidget _regionWidget = region.AddComponent<RegionWidget>();

                _regionWidget.Initialize();
                _regionWidget.UpdateInfo(Manager.Data.weatherData[i].location, Manager.Data.weatherData[i].latitude.ToString(), Manager.Data.weatherData[i].longitude.ToString());
            }
            addRegion.transform.SetAsLastSibling();
        }

        #endregion

        #region externals

        public void OpenRegionPanel()
        {
            _regionPanel.SetActive(true);
            UpdateRegionDisplay();
        }

        public void OnPageChange(int index)
        {
            if (_scrollIndex != index && index < Manager.Data.weatherData.Count) 
            {
                Manager.Popup.EnableSpinner();
                _scrollIndex = index;
                RefreshQuery();
                //SwapDisplay(_scrollIndex);
            }
        }

        public void JumpToFirst()
        {
            _scrollIndex = 0;
            _weatherInfoParent.parent.parent.GetComponent<ScrollRect>().horizontalNormalizedPosition = 0;
        }

        #endregion
    }
}
